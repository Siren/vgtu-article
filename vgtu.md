# VILNIUS TECH Attempted To Lock Out Its Own Students Who Refused Proprietary Software
**Vilnius Gediminas Technical University made its platforms inaccessible to its students by enforcing proprietary and privacy-compromising 2FA methods. The students demanded open standards and free software.**

## UPDATE 2023-04-26
The university has locked out its students again. Without notice as usual.

```
Subject: University blocked students from e-mail AGAIN!
Date: 2023-04-23
From: Irem Kuyucu <siren@kernal.eu>
To: tsc@vilniustech.lt, zivile.sedereviciute-paciauskiene@vilniustech.lt,
    pagalba@vilniustech.lt, justinas.rastenis@vilniustech.lt,
    simona.ramanauskaite@vilniustech.lt

Hello,

Several students who couldn't setup 2FA (two factor authentication) were locked
out of their university email accounts, yet again. That includes me and this is
why I'm contacting you through here and not the university email account.

We have made our points clear that the university didn't implement 2FA properly
and is discriminating against us students who don't consent to sharing personal
data with Microsoft or don't have devices that support Microsoft Authenticator
or refuse to install it because it's proprietary software/spyware.

Free Software Foundation Europe and others publicly criticized the university
and talked about its implementation of proprietary 2FA. See their post about
Vilnius Tech in case you missed it:

https://fsfe.org/news/2023/news-20230418-01.en.html

The post is quite new, still it's been shared on many outlets and social media
already:
https://www.reddit.com/r/europe/comments/12tw0hv/lithuania_students_stop_university_from_using/
https://news.ycombinator.com/item?id=35643915

The students who complained were silently whitelisted from the 2FA requirement
without notice, and therefore we used to have access to the university email. I
am on holidays and only yesterday realized we are blocked again. I am asking,
again, for the university to either implement proper, standard 2FA (TOTP option,
allow other authenticators) now or whitelist us until this problem is resolved.

The world is watching what will the university do next. The university will
either be known for supporting its students or it will be known for
discriminating and enforcing unethical software with little knowledge on how
things should be implemented. The post will be updated based on what happens
next too, of course.
--

Subject: RE: University blocked students from e-mail AGAIN!
Date: 2023-04-24
From: Justinas Rastenis <justinas.rastenis@vilniustech.lt>
To: siren@kernal.eu, tsc@vilniustech.lt,
zivile.sedereviciute-paciauskiene@vilniustech.lt,
    pagalba@vilniustech.lt, justinas.rastenis@vilniustech.lt,
    simona.ramanauskaite@vilniustech.lt

Dear Zehra,

You are a security specialist, we have been talking about the need for 2FA for
two months. We have repeatedly explained why that need and offered you an
alternative to use.

I'm sorry, but I don't know how else to explain it to you.
--

Subject: University blocked students from e-mail AGAIN!
Date: 2023-04-23
From: Irem Kuyucu <siren@kernal.eu>
To: tsc@vilniustech.lt, zivile.sedereviciute-paciauskiene@vilniustech.lt,
    pagalba@vilniustech.lt, justinas.rastenis@vilniustech.lt,
    simona.ramanauskaite@vilniustech.lt

Dear Justinas,

You are a deputy manager. We never asked you to disable 2FA as a whole. We are
asking you to implement it properly before enforcing it. Enable standard 2FA
(TOTP). The problems with the current 2FA requirement are:

- Microsoft Authenticator doesn't support everyone's devices. It's non standard.
- People cannot be forced to give their personal data to Microsoft by using the
  SMS option.

You or the university never offered an alternative or a solution. You only came
up with the idea that we should buy "a second device just for running this
program". No, you need to enable the standard method every single other platform
uses so people can use their password manager of choice. You can still recommend
Microsoft Authenticator to students after that, but do not limit people to it
because it's not even well supported.

You don't seem to understand what is wrong here, in that case I highly suggest
reading FSFE's article:

https://fsfe.org/news/2023/news-20230418-01.en.html

"The use of two factor identification methods helps to secure devices and data
but it should be implemented in a way that is not locking anyone out. VGTU's
mandate for 2FA only gave the option of using proprietary software, raising
concerns to some students who did not want to compromise their privacy. The
university's decision to disable options that would have allowed students to
access their university email using other clients without 2FA was unfair, as it
left students with no options but to use Microsoft Authenticator or to share
their phone number and personal information with Microsoft."
```


## The New University Policy
VILNIUS TECH (Vilnius Gediminas Technical University (VGTU), Lithuanian: Vilniaus Gedimino Technikos Universitetas) is a Lithuanian public university. On 14-02-2023, this e-mail was received by all students and staff:
```
Subject: Svarbūs IT pokyčiai / Important IT changes
Date: 2023-02-14
From: VILNIUS TECH No-Reply <no-reply@vilniustech.lt>

Good day,

Every day Cybercriminals try your and our cyber credulous. Identity thefts
and connections using your account logins became more and more often, that`s
why we need to take actions to save your personal info and forbid to use
your account logins for cyberattacks. One way to do that is to use Two
-factor authentication. That’s why Information Technologies and Systems
center informs you that from March 1st Two-factor authentication will be
turned on for every university account. We strongly recommend to configure
two-factor authentication as soon as possible. After March 1st if you won`t
be configured you will not be able to login to VPN, Studsoft, Microsoft
Teams, Onedrive, Office 365 mail and other services.

Instructions on how to configure yourself:

https://vilniustech.lt/the-centre-of-information-technology-and-systems/it-services-for-vilnius-tech-community/two-factor-authentication/330227?lang=2

Informacinių technologijų ir sistemų centras
Vilniaus Gedimino technikos universitetas
```
### The Problem
There's nothing wrong with enforcing two-factor authenticaton (2FA). However the methods students can configure to enable 2FA are: Microsoft Authenticator (app notifications) and SMS.

Microsoft Authenticator is proprietary software, [proprietary software is often malware](https://www.gnu.org/proprietary/malware-microsoft.html). However, the problem doesn't end here. Only two platforms are supported by this application: Android with Google Play Services or iOS. Not all people have devices that can run this application even if they didn't mind proprietary software. The university expects these people to use the SMS method, which requires the user to share their phone number, personal information, with Microsoft. A company that is notorious for collecting data, violating user privacy and rights. It's simply absurd to be forced to share your personal information with a third party, a corporation based in US, in order to be able to study at a university. This option wasn't appropriate for people either.

Another issue that was raised with this new policy was that the study agreement does not require the students to have a working phone that runs Google Play Services or iOS and a phone plan.

### Attempts at Evasion
Unfortunately, people who care about software freedoms or privacy are often used to being let down and discriminated by public and private institutions. At first, the students tried to evade configuring 2FA so they can keep accessing the platforms essential for their studies.

The students tried to configure app passwords, which allow the university e-mail to be accessed using other clients without 2FA. This option was deliberately disabled by the university. Another thing the students looked for was the option to "Configure app without notifications". This option would allow students to configure TOTP (RFC 6238) and use their own password managers/authenticators. This option also was disabled by the IT staff.

The students even tried to reverse engineer Microsoft Authenticator in hopes of extracting some sort of a secret for OTP. But this wasn't possible because the app sends requests to Microsoft to let it know the app really was installed and configured. It wasn't possible to proceed without this step happening. This is non-standard behavior implemented by Microsoft to lock users down into their application. The students' times were wasted by this.

VILNIUS TECH really made sure that students and staff are stuck with Microsoft.

## Letting The University Know
It's advised to check the complaint policy of the university before taking action but it wasn't possible to find in the case of VILNIUS TECH.

### Contacting The IT Helpdesk
Several students contacted the IT helpdesk to request time-based one-time password (TOTP) option to be enabled. One conversation reads:
```
Date: 2023-02-14
From: Kloud <name.surname@stud.vilniustech.lt>
To: VGTU IT Help <pagalba@vilniustech.lt>

Hi. I recently got an email telling me to enable two-factor authentication
in my vilniustech account, but when I went to enable it there was no option
to use a one-time code (TOPT) instead of a notification. I kindly ask you to
enable it from your azure active directory admin panel for everybody's
convenience, because some people(like me) store their passwords and two-
factor tokens in one place (any password manager. e.g. keepass) and it is
very inconvenient to use a separate app for just one account specifically.

I am attaching some screenshoth of how it should be (white theme
screenshots) vs how it is (dark theme screenshots). I am also attaching official
Microsoft documentation on how to do it for your convenience (the thing
that's disabled is "Verification code from mobile app or hardware token").
--

Date: 2023-02-14
From: VGTU IT Help <pagalba@vilniustech.lt>
To: Kloud <name.surname@stud.vilniustech.lt>

Hello, 

In this case OATH verification codes (One time passwords) are generated
every 30 seconds therefore you will not be able to use them with Password
managers. Regarding software or hardware tokens - we do not use them, therefore this
option was disabled.

Also, since we cannot ensure that personal devices are free of malicious
code, we ask users to approve each and every authentication attempt. We do
not plan to enable a feature where you are able to "Remember" MFA on devices
that You would consider trusted.
--
```
The above e-mail makes it clear that the IT staff of the VILNIUS TECH doesn't know how OTP works. We now know that people who don't understand the standard way is trying to enforce a worse solution by Microsoft, great.
```
Date: 2023-02-15
From: Kloud <name.surname@stud.vilniustech.lt>
To: VGTU IT Help <pagalba@vilniustech.lt>

Yes, I am aware that verification codes are generated every 30 seconds, most
passwords manages can generate them for you from the "secret" (i think you
know what I am talking about), so you can use them with passwords managers.
(I am attaching a screenshot from keepass, the number on the right is a
generated one-time code). I understand that you want users to approve every
authentication attempt, but I am not asking you to enable trusted devices
feature, I am asking you to enable one-time code authentication, it provides
the same functionality and better security as Microsoft authenticator and is
way more convenient. Please let the end user decide which two-factor method
to use.
--

Date: 2023-02-15
From: VGTU IT Help <pagalba@vilniustech.lt>
To: Kloud <name.surname@stud.vilniustech.lt>

Hello,

Neither verification codes nor hardware tokens are used by us. Our systems
only accept verification by using Call or a Notification using the
Authenticator app, Verification codes are not used nor will be supported by
our systems in the near future.
--

Date: 2023-02-22
From: Kloud <name.surname@stud.vilniustech.lt>
To: VGTU IT Help <pagalba@vilniustech.lt>

Hello.

Well, it's not really smart of you. You are basically rejecting the most
widely used, platform independent, convenient 2-factor authentication
method. This decision just shows your incompetence and limits the usability of
university network. I really hope you change your mind.
--

Date: 2023-02-22
From: VGTU IT Help <pagalba@vilniustech.lt>
To: Kloud <name.surname@stud.vilniustech.lt>

Thanks for the suggestion to authenticate using TOPT, but our systems are
not designed for such authentication, so firstly a process of recreating
main university systems has to start. Currently, two-factor authentication
options are available: call and Microsoft app and it works with all
resources. In the future, after vendor updates and all university systems will be
reconfigured - TOPT use will be strongly considered.
--

Date: 2023-03-12
From: Kloud <name.surname@stud.vilniustech.lt>
To: VGTU IT Help <pagalba@vilniustech.lt>

Sorry, I don't understand how can your system not support the standard way
of 2 factor authentication and why do you need to recreate entire system
just to enable 2fa. You are literally using Microsoft Active directory, that
piece of software has this option. I want to be able to authenticate from my
laptop without downloading Microsoft authenticator. Or let's say I have a
phone that doesn't support Microsoft authenticator because it has no google
services, what are the options then?
--

Date: 2023-03-12
From: VGTU IT Help <pagalba@vilniustech.lt>
To: Kloud <name.surname@stud.vilniustech.lt>

To use TOTP we need to reconfigure more than one system because they work
differently or 2FA was not thought of when they were designed. We can't
change systems suddenly, some won't support TOTP even today, which means we
need to find a new solution. Thanks for the comments, but We can't change
anything at this time.
```

Other students received identical or similar replies. E-mailing the IT helpdesk didn't help.

### Emailing Deputy Manager (Justinas), Students Office (Dovile) and Department of Information Technologies (Simona)
Students decided to email other people in the university. The professor whom she e-mailed, Simona, helped with dual-booting university computers with GNU/Linux last year. The IT staff got rid of GNU/Linux this year, they only offer Microsoft Windows once again.

```
Subject: Concerns regarding 2FA
Date: 2023-02-26
From: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>
To: Simona Ramanauskaitė <simona.ramanauskaite@vilniustech.lt>

Hello and I hope you're having a good evening,

I'm not sure if I can voice my concerns about this topic to you, so I
apologise if this isn't the right place. Our university is going to force
people to use 2FA (two factor authentication) starting March. While I have
no problems with this, the options we have for the 2FA are terrible. The
students are forced to give their phone numbers to Microsoft or download the
Microsoft Authenticator application which is only available on Android and
iOS (no desktop version). This is terrible for our privacy and it requires
that we have a phone with us at all times, and also it requires us to have a
mobile phone that supports this specific application.

The better and the standard way to have 2FA is called TOTP. Every major
website and online platform supports TOTP. If the university will allow TOTP
option, the students will be able to use 2FA regardless of any application
or operating system. To briefly explain TOTP:

 1. The website supplies the user with a private key (it's a short string).
This key is never shown again.
 2. The user imports this short string into an application. They can choose
any password manager. Some examples are Nordpass, Google Authenticator,
KeepassXC, and even Microsoft Authenticator if they prefer it. There's no
limitation to a single platform or an operating system.
 3. When logging in the user generates short-lived codes using their app of
choice.

This isn't more complex than the Microsoft Authenticator setup. Microsoft
Authenticator works the same way but it is worse, it locks the user to their
specific platform and it is centralized. There's no reason to disallow TOTP
and make the university systems less accessible. Me and many other students
will not be able to access the university email and other places 2FA will be
required starting from March, if they insist on not providing TOTP. I have
already voiced my concerns, perhaps in a harsher way to the IT department
however they are yet to respond. I believe many students already complained
about this issue.
--

Date: 2023-02-26
From: Simona Ramanauskaitė <simona.ramanauskaite@vilniustech.lt>
To: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>

Hello,

i resent your message to ITSC and they mentioned they will answer to your
email. As well there is a chance ITSC will organize some meeting for explaining
VILNIUS TECH IT infrastructure specifics and discussing your ideas. But you
will see what they will reply to you :)
--

Date: 2023-02-27
From: Justinas Rastenis <justinas.rastenis@vilniustech.lt>
To: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>,
    Simona Ramanauskaitė <simona.ramanauskaite@vilniustech.lt>

Dear Zehra,

Thanks for the suggestion to authenticate using TOPT, but our systems are 
not designed for such authentication, so firstly process of recreating main 
university systems has to start . Currently, two-factor authentication 
options are available: call and Microsoft app and it works with all 
resources. In the future, after vendors updates and all university systems will be 
reconfigured TOPT use will be strongly considered.
--

Date: 2023-02-27
From: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>
To: Justinas Rastenis <justinas.rastenis@vilniustech.lt>,
    Simona Ramanauskaitė <simona.ramanauskaite@vilniustech.lt>

Dear Justinas,

My phone does not support the Microsoft app. I don't have a way of
installing this application. Many other students complained about similar
problems. I will not compromise on my privacy and hand out my phone number to Microsoft
either. Would you be willing to supply me with a burner phone or disable 2FA on my
account? Essentially what you're saying is some people will lose access to
everything that is behind 2FA until someone from the IT department can login
to the Microsoft panel and tick a box that allows TOTP.

It makes no sense that you need to recreate university systems for this
because you aren't the ones handling 2FA. I fail to see how vendor updates
are related to any of this either. You need to configure this using the
Microsoft Active Directory panel. Apart from that, IT department has told
another student that hardware and software keys were disabled voluntarily.
Please see these links for more information:

https://learn.microsoft.com/en-us/azure/active-directory/authentication/concept-authentication-methods-manage
https://learn.microsoft.com/en-us/azure/active-directory/authentication/tutorial-enable-azure-mfa
https://learn.microsoft.com/en-us/azure/active-directory/authentication/howto-authentication-passwordless-security-key#enable-fido2-security-key-method
--

Date: 2023-02-27
From: Justinas Rastenis <justinas.rastenis@vilniustech.lt>
To: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>,
    Simona Ramanauskaitė <simona.ramanauskaite@vilniustech.lt>

Dear Zehra,

I’m sorry, but you don’t hear what I say, services, softwares are not 
support TOTP. How you able to logon to it? Is not a problem to enable TOTP, 
but how to you use it, if not supporting application TOTP authentication 
method?
 
You don’t understanding us, I every month or some times every day, get 
notification, what someone account is was stolen or Police department 
contact with me, that some students accounts credentials was find in someone 
database.

You may be in responsible, but there are over 10,000 students and staff and 
there is certainly not one who does not understand security and can press 
anything to get the job done as quickly as possible.

Please read the articles where we reveal how unsafe it is and how to take 
action.

https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8732169

Against insecure users, two factors not only protect them from identity 
stolen, but also from hacking systems.

What phone do you have that doesn't support 2FA applications?
--
```
It's quite hilarious that the deputy manager replies with an [article](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8732169) about "phishing attacks" that himself and the professor authored together, which is completely irrelevant to the discussion. People who don't even understand how standard 2FA works should not explain "how to take action" to others.
```
Date: 2023-02-28
From: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>
To: Justinas Rastenis <justinas.rastenis@vilniustech.lt>,
    Simona Ramanauskaitė <simona.ramanauskaite@vilniustech.lt>

Dear Justinas,

I'm not asking you to disable 2FA. I'm asking you to allow using TOTP with 
programs other than Microsoft Authenticator. There is a setting that enables 
TOTP usage with other authenticator programs. I'm asking you to not limit 
TOTP to Microsoft Authenticator. You have this extra option disabled at the 
moment and that is causing people problems. Since enabling this isn't a 
problem for you, please do that.

I have a PinePhone running PostmarketOS. My phone doesn't have Google Play 
Store or Apple Store. It cannot install and run Microsoft Authenticator. I 
use KeepassXC program to manage TOTP and passwords on my phone.

Many people are already using password managers other than Microsoft 
Authenticator. If you allow TOTP to be used with other authenticator 
programs as well, everyone will be able to access university systems. TOTP 
is not specific to Microsoft Authenticator. TOTP is a standard supported by 
many password managers.
--

Date: 2023-03-08
From: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>
To: Justinas Rastenis <justinas.rastenis@vilniustech.lt>,
    Simona Ramanauskaitė <simona.ramanauskaite@vilniustech.lt>

Hello,

I see that you still haven't resolved this issue. Therefore I will be
explaining the steps on how to do this:

    Go to Security > Multifactor Authentication > Additional cloud-based
multifactor authentication settings.
    Tick the checkboxes like in the attached image.

If you lock me out of the university e-mail account, I will consider taking 
legal action. You cannot discriminate against students because they refuse 
to/cannot install a properitary application on their personal devices. And 
you cannot discriminate against students because they don't want to provide 
their personal information to Microsoft. Especially when there are other 
verification options that allow everyone to access the systems, and that you 
voluntarily have these options disabled.
--
```
According to Lithuanian law educational establishements are not allowed to discriminate students based on their social status or beliefs. Students were thinking of reporting this situation to Office of the Equal Opportunities Ombudsperson in Lithuania at the time.

```
Date: 2023-03-09
From: Justinas Rastenis <justinas.rastenis@vilniustech.lt>
To: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>,
    Simona Ramanauskaitė <simona.ramanauskaite@vilniustech.lt>,
    Dovilė Jodenytė <dovile.jodenyte@vilniustech.lt>

Dear Zehra,

You don’t understand what I say to you. Please hear, what I want to say to
you. Our systems and services does not support TOTP authentication method, you 
will be not able login to systems and services.

If your phone doesn’t support Microsoft Authenticator, you need to use “Call 
to phone”, if you don’t want that method to use, you need to change your 
phone, which support Microsoft.
```
The deputy manager decides to CC international students office in his reply. His recommendation to the student is absolutely hilarious: "Just buy another device that supports this one application, or hand out your data".
```
Date: 2023-03-09
From: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>
To: Justinas Rastenis <justinas.rastenis@vilniustech.lt>,
    Simona Ramanauskaitė <simona.ramanauskaite@vilniustech.lt>,
    Dovilė Jodenytė <dovile.jodenyte@vilniustech.lt>

Dear Justinas,

Have you checked the instructions that I have given you, or the links that 
describe how to do this? I'm not going to buy a second phone for this and I 
will not give my personal phone number to Microsoft. The two-factor 
authentication methods have nothing to do with your systems or services 
because you're using Azure Active Directory. If you were able to enable 
Microsoft Authenticator so far, you're also able to tick on that checkbox 
that allows other authenticators.

Please supply me with a phone or disable 2FA on my account if you keep 
insisting on this. Offer me a solution that will not make me buy a second 
device or violate my own privacy.
--

Date: 2023-03-09
From: Justinas Rastenis <justinas.rastenis@vilniustech.lt>
To: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>,
    Simona Ramanauskaitė <simona.ramanauskaite@vilniustech.lt>,
    Dovilė Jodenytė <dovile.jodenyte@vilniustech.lt>

Dear Zehra,

I checked your instruction, but you will not able to login to our system if 
you use TOTH method, our system are not supporting TOTH method.

We use not one Office 365 services, we use VPN, StudSoft and another systems 
and services which do not support the TOTH method. 

If I turn on, you will not able to login to us.
--

Date: 2023-03-09
From: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>
To: Justinas Rastenis <justinas.rastenis@vilniustech.lt>,
    Simona Ramanauskaitė <simona.ramanauskaite@vilniustech.lt>,
    Dovilė Jodenytė <dovile.jodenyte@vilniustech.lt>

Dear Justinas,

I can't use software from StudSoft or FortiClient VPN because they do not support my desktop OS (Linux) anyways. I highly doubt that many others use them either. I know for a fact that nobody in my course has even interacted with these systems once. The only university systems/resources I use are Moodle and Office 365 services. Not being able to log onto the places you mentioned isn't a problem for me. So please, I'd appreciate it if you could either enable TOTP or disable 2FA for my account.
--

Date: 2023-03-10
From: Justinas Rastenis <justinas.rastenis@vilniustech.lt>
To: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>,
    Simona Ramanauskaitė <simona.ramanauskaite@vilniustech.lt>,
    Dovilė Jodenytė <dovile.jodenyte@vilniustech.lt>

If you have Linux OS, you need use virtual machine to connect Windows 
services. Fortigate has Linux OS client.

2FA must be used and there will be no discussion here about turning it off, 
you can use it, and if you don't want to do it, that's up to you.
```
It's extremely frustrating to see how the university constantly ignored the problem at hand and tried to divert the topic into other places instead of offering a solution.
```
Date: 2023-03-10
From: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>
To: Justinas Rastenis <justinas.rastenis@vilniustech.lt>,
    Simona Ramanauskaitė <simona.ramanauskaite@vilniustech.lt>,
    Dovilė Jodenytė <dovile.jodenyte@vilniustech.lt>

I don't use these services so I don't care. Only email and Moodle are 
essential to my studies. And no I cannot use your options for 2FA either as 
I explained earlier.

Another thing you can do to help is enabling app passwords. So people who 
want to use the email but cannot use your 2FA methods can still continue 
using the email. 2FA will break people's email client setups. If you enable 
app passwords I will be able to continue using the e-mail using Thunderbird 
(an external email client) for example.

Instructions on how to enable app passwords:
https://learn.microsoft.com/en-us/azure/active-directory/authentication/howto-mfa-app-passwords
--

Date: 2023-03-12
From: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>
To: Justinas Rastenis <justinas.rastenis@vilniustech.lt>,
    Simona Ramanauskaitė <simona.ramanauskaite@vilniustech.lt>,
    Dovilė Jodenytė <dovile.jodenyte@vilniustech.lt>

Hi,

As far as I am aware there isn't a requirement for students of this 
university to have a working mobile phone. This isn't part of the student 
commitments in my study agreement. You cannot require people to have a phone 
or a phone number. Either way I would like to remind you, why you must offer 
another option for 2FA (that isn't SMS or Microsoft Authenticator):

   - Microsoft Authenticator is proprietary software, therefore it is a 
privacy and security risk. I don't agree with the terms of service or the 
EULA because it is invasive. If TOTP was allowed, people could use any 
authenticator they want, even the Microsoft Authenticator if they wish.
   - Microsoft Authenticator requires Google Play Services or iOS. People 
with devices that don't support both exist. You're removing their access to 
university services.
   - SMS option requires people to give their personal data (phone numbers) 
to a corporation based in USA, Microsoft. I do not consent to sharing my 
personal data with such a company.

It isn't my problem that you made the mistake to only support Microsoft 
Authenticator for StudSoft or the VPN. Simply tell the users of these 
services to use Microsoft Authenticator or SMS instead of removing access of 
other people who cannot use these methods. You had the option to support 
everyone in this university by allowing the standard TOTP method and you 
didn't. I am tired of trying to explain what is so wrong here when it is so 
obvious. Why must the students suffer the consequences of these bad decisions?

You cannot revoke some students' access, force people to download a 
proprietary application on their personal devices and tell people to buy new 
phones. There is clearly something wrong here.

I must have access to university email for my studies. If you remove my 
access, I will elevate this issue to the responsible entities regarding the 
discrimination that is occurring here, in an educational institution.
--

Date: 2023-03-13
From: Justinas Rastenis <justinas.rastenis@vilniustech.lt>
To: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>,
    Simona Ramanauskaitė <simona.ramanauskaite@vilniustech.lt>,
    Dovilė Jodenytė <dovile.jodenyte@vilniustech.lt>

Life changes, what happened yesterday may not exist tomorrow.

Many students are irresponsible, they click on everything, their computers 
are infected with viruses, their accounts are often stolen. Please advise 
what to do with such irresponsible users? Blocking, warnings them, do not 
help.

We want to protect your identity and protect our systems from hacker 
attacks.

You have two choices, install the application or receive a call.
 
I think all students have a phone number where they can be called.

After configuring e-mail on the phone or computer mail, its verification 
requires two-factor authentication quite rarely. 

It is Microsoft's responsibility to take care of your data, this is 
regulated by the GPDR and the new regulation NIS 2.
--

Date: 2023-03-13
From: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>
To: Justinas Rastenis <justinas.rastenis@vilniustech.lt>,
    Simona Ramanauskaitė <simona.ramanauskaite@vilniustech.lt>,
    Dovilė Jodenytė <dovile.jodenyte@vilniustech.lt>

You're not asked to disable 2FA as a whole. You're asked to allow TOTP or 
app passwords along with the options you currently offer. TOTP in fact is 
the most secure option that you could possibly offer. Proprietary software 
such as Microsoft Authenticator is a security and privacy risk. Tell users 
who cannot use the platforms you mentioned with TOTP to use SMS or Microsoft 
Authenticator instead. That is the way to go.

Why are you talking about unrelated topics? Instead offer me a solution. I 
cannot install the app and for privacy and security reasons I will not 
provide my personal information to Microsoft. Microsoft products come with 
many security vulnerabilities, they contain backdoors. Microsoft products 
are malware. Read more on that here: https://www.gnu.org/proprietary/
malware-microsoft.html

For your information I am a cyber security analyst who works full time. As 
for your question what to do with such users?

    Do not force people to use proprietary software such as Microsoft 
Authenticator and Zoom. Remember if software is closed source, the source is 
to be ashamed off.

  - Do not force people to share their personal information with third party 
companies.
  - Fix your password policy first. It is proven that forcing users to 
change their passwords so often makes them pick weak passwords. Most people 
have two weak passwords and they revert to one of these every time you force 
them to change it.
  - Do not limit the allowed characters in people's passwords. You're even 
showing the allowed character set on the website. You're narrowing down an 
attacker's wordlist when bruteforcing passwords.
  - Fix your wireless network security. Client certificate authentication is 
a must. You do not have this. The university network is prone to many 
attacks such as the evil twin.

"It is Microsoft's responsibility to take care of your data, this is 
regulated by the GPDR and the new regulation NIS 2." yes if you're forcing 
people to use these options it is. The problem is I do not trust Microsoft 
with that nor I consent to that.

"I think all students have a phone number where they can be called." it 
doesn't matter what you or someone else thinks. You cannot legally force 
someone to share their personal information. University policy doesn't 
require students to have a working mobile phone.

Your available options are:

  - Enable TOTP.
  - Enable app passwords.
  - Supply students who cannot use/don't consent with mobile devices.

Just enable TOTP and tell those people who use the VPN or StudSoft to stick 
to SMS or MS Authenticator. How hard is that?
```
The frustrated student is highlighting university's poor security decisions when asked "Please advise what to do with such irresponsible users?" in a thread about enabling TOTP. Clearly the university doesn't have a good sense of security.
```
Date: 2023-03-14
From: Justinas Rastenis <justinas.rastenis@vilniustech.lt>
To: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>,
    Simona Ramanauskaitė <simona.ramanauskaite@vilniustech.lt>,
    Dovilė Jodenytė <dovile.jodenyte@vilniustech.lt>

To use TOTP we need to reconfigure more than one system because they work 
differently or 2FA was not thought of when they were designed. We can't 
change systems suddenly, some won't support TOTP even today, which means we 
need to find a new solution.

Thanks for the comments, but I can't change anything at this time.
```
After brushing off all of the complaints and concerns with "Thanks for the comments, but I can't change anything at this time.", the conversation ends here.
### Emailing Department of Information Systems
After receiving a recommendation from someone that these people work in private sector and they might be able to understand the issue better, the students tried to contact here. Goranin is the head of the department and Mažeika is a Chief Research Fellow.
```
Date: 2023-03-14
From: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>
To: Nikolaj Goranin <nikolaj.goranin@vilniustech.lt>
    Dalius Mažeika <dalius.mazeika@vilniustech.lt>

Good evening,

I was referred to you by a colleague. I would like to discuss a problem 
regarding our university infrastructure with you.

As you have probably seen the university started to enforce 2FA. The problem 
with this is that, non-standard methods for 2FA are offered. I reached out 
to the IT department of the university with no avail. I asked for TOTP to be 
enabled, however they refused and it sounds like they don't know how it 
works. 

There are students with devices that doesn't have Google Play Services or 
iOS. Therefore Microsoft Authenticator cannot be installed on their devices. 
Students aren't required to have a mobile phone suitable for running the 
Microsoft Authenticator app in order to study in this university. This isn't 
a part of student's commitments in our study agreements or study subject 
requirements. These students will be locked out of university systems that require 2FA 
(including email and office365) in a couple of days if they refuse to 
provide their personal data to Microsoft. 

The other offered method for 2FA involves sharing of personal data (phone 
numbers) with Microsoft. People cannot be forced to consent to do this.

Simply allowing standard TOTP would support every single students in this 
university. Then people can use their authenticator app of choice. Forcing people to use 
proprietary software that doesn't even have widespread support or forcing 
people to share their personal information with third party companies to 
access university resources is unacceptable. 

I would really appreciate it if you could somehow help with this situation. 
Maybe you could explain the problem better to the IT department.
--

Date: 2023-03-15
From: Nikolaj Goranin <nikolaj.goranin@vilniustech.lt>
To: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>
    Dalius Mažeika <dalius.mazeika@vilniustech.lt>

Dear Irem,

I understand your concern, but this is the decision made by the IT 
department and approved by the central administration. If you disagree with 
this decision I suggest you contacting the Vice-Rector for Studies.
--

Date: 2023-03-15
From: Dalius Mažeika <dalius.mazeika@vilniustech.lt>
To: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>
    Nikolaj Goranin <nikolaj.goranin@vilniustech.lt>

Dear Irem

2FA was decided to use in order to protect our IT infrastructure from cyber 
attacks that are happened constantly during last years. VilniusTech fully 
trust Microsoft policy for private data protection.

https://www.microsoft.com/en-us/trust-center/privacy

I agree that usage of 2FA using personal phone was not included in study 
contract but it is mentioned in your contract that students must follow 
VilniusTech internal regulations and policies.
--

Date: 2023-03-20
From: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>
To: Dalius Mažeika <dalius.mazeika@vilniustech.lt>

Dear Prof. Dr. Mažeika,

I don't have a problem with 2FA being enabled. However the options offered 
for 2FA are non-standard, unsecure and obscure. TOTP must be offered to 
support all students and respect our privacy. This policy is discriminatory 
otherwise.

I don't have a device that supports Microsoft Authenticator. However if TOTP 
was enabled, I could use KeepassXC on my desktop for 2FA. It is a horrible 
decision to limit people to a non-standard proprietary application. I don't 
agree with its terms of use/EULA either. I'm not going to provide my 
personal information to an advertisement company with known horrible 
practices either. I am going to be locked out of my university office365 
account in two days. I was hoping the university would be more privacy aware 
and understanding of user rights and freedoms. Instead of being told to "buy 
a second phone" which I will not do because it's not about money, it's about 
user freedom.

I'm a person who works full-time in Lithuania as a penetration tester. I was 
awarded full scholarship by this university. I also always had grade 
averages of 10-8. It's a shame that this university is risking loss of its 
students. I will be looking to transfer out of this university if this issue anyhow 
starts affecting my grades, if lecturers/staff isn't okay with my personal 
email usage. I'm only telling you this because I mostly enjoyed my time in 
this university. The quality of lectures and the atmosphere is great. 
However, I'm very upset that this decision is an obstacle to my studies.
```
The conversation dies here with no more replies.

### Emailing The Vice Rector for Studies
```
Date: 2023-03-15
From: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>
To: Živilė Sederevičiūtė-Pačiauskienė <zivile.sedereviciute-paciauskiene@vilniustech.lt>

Dear Prof. Dr. Živilė Sederevičiūtė-Pačiauskienė,

I hope you're having a lovely evening. As you have probably seen, the 
university started to enforce two factor authentication (2FA). This is great 
however, the methods offered for 2FA are non-standard. I reached out to the 
IT department to explain them the issue with no avail. I asked for the most 
widespread and secure method, the TOTP standard to be enabled, however they 
refused and it sounds like they don't understand how it all works. I know 
for a fact that the university infrastructure supports this method because 
they use Azure Active Directory service for authentication.

There are students with devices that doesn't have Google Play Services or 
iOS. Therefore Microsoft Authenticator cannot be installed on their devices. 
In fact many students have been complaining about this to the IT department 
in the past month. Not that there doesn't exist people who can install the 
app, however they refuse to do this out of privacy concerns because the app 
is proprietary software vendored by a company with unethical practices. All 
of these students will be locked out of university systems that require 2FA 
(including email and office365) in a couple of days if they refuse to 
provide their personal data to Microsoft. Simply because the university is 
deliberately choosing to not support their devices.

The other offered method for 2FA involves sharing of personal information 
(phone numbers) with Microsoft. People cannot be forced to consent to do 
this. I believe that students shouldn't be discriminated against in such a way, 
especially when there is the TOTP option which is a widely accepted 
standard, which will support everybody regardless of their device or 
operating system. Then people can use their authenticator app of choice, 
like Nordpass, Google authenticator, KeepassXC and even the non-standard 
Microsoft Authenticator if they wish. There is no negative security 
implications of allowing TOTP, the underlying mechanism of Microsoft 
Authenticator is the same. However Microsoft is restricting the user to 
their own authenticator program that has little support, and making it 
impossible to migrate out of their application or configure other 
authenticators.

Forcing people to use proprietary software that doesn't even have widespread 
support or forcing people to share their personal information with third 
party companies to access university resources is unacceptable and 
unethical. This shouldn't be an obstacle to our education. Therefore I'm asking for TOTP 
(other authenticator support) to be enabled. If it will not be enabled, I'm 
one of those students who will eventually lose access to the university e-
mail and other places. I am worried because some lecturers only use e-mail 
to receive/send homeworks and course material, so having access to e-mail is 
essential to my studies.

I would really appreciate it if you could somehow help with this situation. 
I was referred to you after explaining this issue to several people from 
different departments. They understand the problem, however they cannot do 
anything about it as this was a decision approved by the central 
administration.

If you would like to know more about why people hesitate or outright refuse 
to install proprietary (non-free) software such as Microsoft Authenticator, 
please see [here](https://www.gnu.org/proprietary/proprietary.html).

There is also a page dedicated to Microsoft's insecurity and abuse [here](https://www.gnu.org/proprietary/malware-microsoft.html).
```
The vice-rector for studies didn't reply to this e-mail.

## The University Backs Up From Their Decision Without Telling
After blocking students who couldn't configure 2FA for about a week, it was discovered on 2023-03-27 that students could access e-mail again. Nobody was notified of this change. And to this day students still have access without 2FA. The university doesn't offer OTP option for 2FA yet.

## Bonus: Student Hacks VILNIUS TECH and Proceeds To Tell Them About It Using An Account They Tried to Lock Her out of
Shortly after VILNIUS TECH reverted their mandatory 2FA policy, one of the disgruntled students hacked the university's GitLab instance.

Despite being two months old, the instance was running an old version with critical vulnerabilities. The student managed to exploit CVE-2022-2884 and got shell access to the server. Later on she managed to grant herself admin rights on GitLab using `gitlab-rails console`. She decided to not go any further and report at this stage.

```
Subject: Update GitLab Please
Date: 2023-03-31
From: Zehra Irem Kuyucu <zehra-irem.kuyucu@stud.vilniustech.lt>
To: VGTU IT Help <pagalba@vilniustech.lt>

Hi,

I pwned your GitLab server and temporarily patched the vulnerability by
disabling GitHub integration. Unfortunately, I don't have root on the
server yet and I cannot upgrade GitLab myself but you should really
consider that some time.

Ps: Thanks for not locking me out of the e-mail, I wouldn't be able to
report this very important issue to you otherwise.
```
![Attached: GitLab Admin Dashboard](https://gitlab.digilol.net/Siren/vgtu-article/-/raw/master/update-asap.png)

Even then, the university did not comment on the decision to not enforce 2FA. This was their reply:
```
Subject: RE: Update GitLab Please
Date: 2023-03-31
From: Justinas Rastenis <justinas.rastenis@vilniustech.lt>
To: Zehra Irem Kuyucu

Thanks for letting us know, but in the future, just let us know there are
bugs and we'll update, because in this case the server is shut down and will
be rebuilt.
```
The dashboard screams "Update ASAP", so they did know there were "bugs". They were simply waiting for someone to exploit it.
